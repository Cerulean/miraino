# -*- coding:utf-8 -*-

import matplotlib.pyplot as plt

type_decision_node = dict(boxstyle="sawtooth", fc="0.8")
type_leaf_node = dict(boxstyle="round4", fc="0.8")
type_arrow = dict(arrowstyle="<-")


def get_leafs_count(tree):

    leafs_count = 0

    root_tag = tree.keys()[0]

    sub_tree = tree[root_tag]

    for tag in sub_tree.keys():

        if isinstance(sub_tree[tag], dict):
            leafs_count += get_leafs_count(sub_tree[tag])
        else:
            leafs_count += 1

    return leafs_count


def get_tree_depth(tree):

    max_depth = 0

    root_tag = tree.keys()[0]

    sub_tree = tree[root_tag]

    for tag in sub_tree.keys():

        if isinstance(sub_tree[tag], dict):
            current_depth = 1 + get_tree_depth(sub_tree[tag])
        else:
            current_depth = 1

        if current_depth > max_depth:
            max_depth = current_depth

    return max_depth


def plot_node(node_text, center_point, parent_point, node_type):

    create_plot.ax1.annotate(node_text,
                             xy=parent_point,
                             xycoords='axes fraction',
                             xytext=center_point,
                             textcoords='axes fraction',
                             va="center",
                             ha="center",
                             bbox=node_type,
                             arrowprops=type_arrow)


def plot_mid_text(center_point, parent_point, text):

    x = (parent_point[0] - center_point[0]) / 2.0 + center_point[0]
    y = (parent_point[1] - center_point[1]) / 2.0 + center_point[1]

    create_plot.ax1.text(x, y, text, va="center", ha="center", rotation=30)


def plot_tree(tree, parent_point, node_text):

    leafs_count = get_leafs_count(tree)

    tree_depth = get_tree_depth(tree)

    root_tag = tree.keys()[0]

    sub_tree = tree[root_tag]

    center_point = (plot_tree.offset_x + (1.0 + float(leafs_count)) / 2.0 / plot_tree.total_width, plot_tree.offset_y)

    plot_mid_text(center_point, parent_point, node_text)

    plot_node(root_tag, center_point, parent_point, type_decision_node)

    plot_tree.offset_y = plot_tree.offset_y - 1.0 / plot_tree.total_depth

    for key in sub_tree.keys():

        if isinstance(sub_tree[key], dict):

            plot_tree(sub_tree[key], center_point, str(key))

        else:

            plot_tree.offset_x = plot_tree.offset_x + 1.0 / plot_tree.total_width

            plot_node(sub_tree[key], (plot_tree.offset_x, plot_tree.offset_y), center_point, type_leaf_node)

            plot_mid_text((plot_tree.offset_x, plot_tree.offset_y), center_point, str(key))

    plot_tree.offset_y = plot_tree.offset_y + 1.0 / plot_tree.total_depth


def create_plot(tree):

    figure = plt.figure(1, facecolor='white')
    figure.clf()

    axis_arguments = dict(xticks=[], yticks=[])

    create_plot.ax1 = plt.subplot(111, frameon=False, **axis_arguments)

    plot_tree.total_width = float(get_leafs_count(tree))
    plot_tree.total_depth = float(get_tree_depth(tree))
    plot_tree.offset_x = -0.5 / plot_tree.total_width
    plot_tree.offset_y = 1.0
    plot_tree(tree, (0.5, 1.0), '')

    plt.show()
