# -*- coding:utf-8 -*-

import operator
import os

from matplotlib import pyplot
from numpy import *


def create_data_set():
    group = array([[1.0, 1.1],
                   [1.0, 1.0],
                   [0.0, 0.0],
                   [0.0, 0.1]])

    labels = ['A', 'A', 'B', 'B']

    return group, labels


def classify(x, training_set, training_labels, k):

    rows, columns = training_set.shape                              # 获取训练集矩阵的行、列数

    offset_matrix = tile(x, (rows, 1)) - training_set               # 将输入向量复制 rows 行，并与训练集矩阵相减

    square_offset_matrix = offset_matrix ** 2                       # 对结果矩阵中每一组行向量求平方

    offset_sum = square_offset_matrix.sum(axis=1) ** 0.5            # 求每一组行向量的和并开方

    sorted_index_of_offset_sum = offset_sum.argsort()               # 对欧氏距离数组的索引从小到大排序

    label_count = {}                                                # 创建标签字典，键为标签名，值为标签出现次数

    for i in range(k):
        label = training_labels[sorted_index_of_offset_sum[i]]      # 获得第k个索引对应的向量的标签
        label_count[label] = label_count.get(label, 0) + 1          # 将第k个索引对应的向量的标签出现次数+1并存储在标签字典

    sorted_class_count = sorted(label_count.iteritems(),            # 对标签字典的值（标签出现次数）逆序排序
                                key=operator.itemgetter(1),
                                reverse=True)

    return sorted_class_count[0][0]                                 # 输出出现次数最大的键值


def file_to_matrix(file_name):

    target_file = open(file_name)

    lines = target_file.readlines()

    target_matrix = zeros((len(lines), 3))

    labels = []

    index = 0

    for line in lines:

        line = line.strip()

        vector = line.split('\t')

        target_matrix[index, :] = vector[0:3]

        labels.append(vector[-1])

        index += 1

    return target_matrix, labels


def image_to_matrix(file_name):

    vector = zeros((1, 1024))

    image = open(file_name)

    for i in range(32):

        line = image.readline()

        for j in range(32):

            vector[0, 32 * i + j] = int(line[j])

    return vector


def auto_normalize(data_set):
    min_values = data_set.min(0)
    max_values = data_set.max(0)

    ranges = max_values - min_values

    rows = data_set.shape[0]

    normalized_data_set = zeros(shape(data_set))

    normalized_data_set = data_set - tile(min_values, (rows, 1))
    normalized_data_set = normalized_data_set / tile(ranges, (rows, 1))

    return normalized_data_set, rows, min_values


def run_dating_test_data():
    data_set, labels = file_to_matrix("./datingTestSet/datingTestSet.txt")

    normalized_data_set, ranges, min_values = auto_normalize(data_set)

    rows = normalized_data_set.shape[0]

    test_data_set_count = int(rows * 0.1)

    error_count = 0.0

    for i in range(test_data_set_count):
        result = classify(normalized_data_set[i, :],
                          normalized_data_set[test_data_set_count: rows, :],
                          labels[test_data_set_count:rows],
                          3)
        print "Classifier answers %s, the truth is %s" % (result, labels[i])

        if result != labels[i]:
            error_count += 1.0

    print "Total error ration is %f" % (error_count / float(test_data_set_count))


def run_digits_test_data():

    labels = []

    training_set_list = os.listdir("./trainingDigits")
    test_set_list = os.listdir("./testDigits")

    training_rows = len(training_set_list)

    training_set_matrix = zeros((training_rows, 1024))

    for i in range(training_rows):

        file_name = training_set_list[i]

        file_prefix = file_name.split(".")[0]

        label = int(file_prefix.split("_")[0])

        labels.append(label)

        training_set_matrix[i, :] = image_to_matrix("./trainingDigits/" + file_name)

    error_count = 0.0

    test_rows = len(test_set_list)

    for i in range(test_rows):

        file_name = test_set_list[i]

        file_prefix = file_name.split(".")[0]

        label = int(file_prefix.split("_")[0])

        image_vector = image_to_matrix("./testDigits/" + file_name)

        result = classify(image_vector, training_set_matrix, labels, 3)

        print "Classifier answers %s, the truth is %s" % (result, label)

        if result != label:
            error_count += 1.0

    print "Total error ration is %f" % (error_count / float(test_rows))


if __name__ == "__main__":
    run_dating_test_data()