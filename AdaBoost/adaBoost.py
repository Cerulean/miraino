# -*- coding:utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

from Base.base import BaseObject


class AdaBoost(BaseObject):

    def __init__(self, training_set, training_labels):

        super(AdaBoost, self).__init__(training_set, training_labels)

        self.meta_decision_stump = DecisionStump(self.training_set, self.training_labels)

        self.decision_stumps = None

    def classify(self, data_set, labels):

        data_mat = np.mat(data_set)

        labels_mat = np.mat(labels).T

        rows = data_mat.shape[0]

        aggregated_result_mat = np.mat(np.zeros((rows, 1)))

        for index in range(len(self.decision_stumps)):

            decision_stump = self.decision_stumps[index]

            col = decision_stump['col']

            alpha = decision_stump['alpha']

            symbol = decision_stump['symbol']

            threshold = decision_stump['threshold']

            result_mat = self.meta_decision_stump.classify(col, threshold, symbol, data_mat)

            aggregated_result_mat += alpha * result_mat

        print "Classify aggregated mat is:", aggregated_result_mat

        error_mat = np.mat(np.ones((rows, 1)))

        error_count = error_mat[np.sign(aggregated_result_mat) != labels_mat].sum()

        error_ratio = error_count / rows

        print "Error ratio is:", error_ratio

        self.plot_roc(np.sign(aggregated_result_mat), labels_mat)

        return np.sign(aggregated_result_mat)

    def train(self, max_iterations):

        decision_stumps = []

        rows = self.training_set.shape[0]

        weights = np.mat(np.ones((rows, 1)) / rows)

        aggregated_best_result_mat = np.mat(np.zeros((rows, 1)))

        for iteration in range(max_iterations):

            print "Iteration:", iteration

            best_stump, min_error_product, best_result_mat = self.meta_decision_stump.create(weights)

            print "Current weights: ", weights.T

            # Alpha is the coefficient of g(x), which is weak decision stump.

            alpha = float(0.5 * np.log((1.0 - min_error_product) / max(min_error_product, 1e-16)))

            best_stump['alpha'] = alpha

            decision_stumps.append(best_stump)

            print "Best result mat: ", best_result_mat.T

            # Here the power will strengthen the label classified that was wrong in next iteration.

            power = np.multiply(-1 * alpha * self.training_labels, best_result_mat)

            reweighs_ratio = np.exp(power)

            weights = np.multiply(weights, reweighs_ratio)

            weights = weights / weights.sum()

            aggregated_best_result_mat += alpha * best_result_mat

            print "Aggregated best result mat: \n", aggregated_best_result_mat

            # Here the error will be 0, but correctness will be 1

            aggregated_bool_result = np.sign(aggregated_best_result_mat) != self.training_labels

            aggregated_error_mat = np.multiply(aggregated_bool_result, np.ones((rows, 1)))

            error_ratio = aggregated_error_mat.sum() / rows

            print "Total error ratio:", error_ratio, "\n"

            if error_ratio == 0.0:

                break

        self.decision_stumps = decision_stumps

    @staticmethod
    def plot_roc(results_mat, labels_mat):

        coordinate = (1.0, 1.0)

        sum_of_y = 0.0

        label_count = len(labels_mat)

        positive_label_count = labels_mat[labels_mat == 1.0].sum()

        step_of_y = 1 / float(positive_label_count)

        step_of_x = 1 / float(label_count - positive_label_count)

        sorted_indices = results_mat.argsort()

        figure = plt.figure()
        figure.clf()

        axis = plt.subplot(111)

        for index in sorted_indices.tolist()[0]:

            if labels_mat[index] == 1.0:

                delta_x, delta_y = 0, step_of_y

            else:

                delta_x, delta_y = step_of_x, 0

                sum_of_y += coordinate[1]

            axis.plot([coordinate[0], coordinate[0] - delta_x], [coordinate[1], coordinate[1] - delta_y], c='b')

            coordinate = (coordinate[0] - delta_x, coordinate[1] - delta_y)

        axis.plot([0, 1], [0, 1], 'b--')

        plt.xlabel("False Positive Ratio")
        plt.ylabel("True Positive Ratio")
        plt.title("ROC Curve")

        axis.axis([0, 1, 0, 1])

        plt.show()

        print "The area under ROC curve is", sum_of_y * step_of_x

    @classmethod
    def load_simple_data(cls):

        data = np.mat([[1.0, 2.1],
                       [2.0, 1.1],
                       [1.3, 1.0],
                       [1.0, 1.0],
                       [2.0, 1.0]])

        labels = [1.0, 1.0, -1.0, -1.0, 1.0]

        return data, labels


class DecisionStump(object):

    def __init__(self, data_set, labels):

        self.data_set = data_set

        self.labels = labels

    def create(self, weights):

        rows, cols = self.data_set.shape

        step_count = 10.0

        best_stump = {}

        best_result_mat = np.mat(np.zeros((rows, 1)))

        min_error_product = np.inf

        for col in range(cols):

            col_min, col_max = self.data_set[:, col].min(), self.data_set[:, col].max()

            step_size = (col_max - col_min) / step_count

            for iteration in range(-1, int(step_count) + 1):

                for symbol in ['lt', 'gt']:

                    threshold = col_min + float(iteration) * step_size

                    # The result_mat is mat with -1 and 1.

                    result_mat = self.classify(col, threshold, symbol)

                    # The error_mat is mat with 1 and 0, 1 means Error.

                    error_mat = np.mat(np.ones((rows, 1)))
                    error_mat[result_mat == self.labels] = 0

                    # Weights.T is 1 * m mat, error_mat is m * 1 mat.

                    weighted_error_product = weights.T * error_mat

                    # print "Col: %d, threshold: %.2f, symbol: %s, weighted error is %.3f" % (col,
                    #                                                                         threshold,
                    #                                                                         symbol,
                    #                                                                         weighted_error_product)
                    # AdaBoost wants minimal weighted error product.

                    if weighted_error_product < min_error_product:

                        min_error_product = weighted_error_product

                        best_result_mat = result_mat.copy()

                        best_stump['col'] = col
                        best_stump['symbol'] = symbol
                        best_stump['threshold'] = threshold

        return best_stump, min_error_product, best_result_mat

    def classify(self, col, threshold, symbol, data_set=None):

        if data_set is None:
            data_set = self.data_set

        rows = data_set.shape[0]

        result_mat = np.ones((rows, 1))

        if symbol == 'lt':

            result_mat[data_set[:, col] <= threshold] = -1.0

        else:

            result_mat[data_set[:, col] > threshold] = -1.0

        return np.mat(result_mat)


if __name__ == '__main__':

    _training_set, _training_labels = AdaBoost.load_data_set("./sample/horseColicTraining2.txt")

    test_set, test_labels = AdaBoost.load_data_set("./sample/horseColicTest2.txt")

    adaBoost = AdaBoost(_training_set, _training_labels)
    adaBoost.train(20)

    adaBoost.classify(test_set, test_labels)
