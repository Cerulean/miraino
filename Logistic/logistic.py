# -*- coding:utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import random


def plot(weights):

    data_set, labels = load_data_set()

    data_set = np.array(data_set)

    rows = np.shape(data_set)[0]

    x1_cords_0, x2_cords_0 = [], []
    x1_cords_1, x2_cords_1 = [], []

    for row in range(rows):

        if int(labels[row]) == 1:

            x1_cords_0.append(data_set[row, 1])
            x2_cords_0.append(data_set[row, 2])

        else:

            x1_cords_1.append(data_set[row, 1])
            x2_cords_1.append(data_set[row, 2])

    figure = plt.figure()

    axis = figure.add_subplot(111)

    axis.scatter(x1_cords_0, x2_cords_0, s=30, c='red', marker='s')
    axis.scatter(x1_cords_1, x2_cords_1, s=30, c='green')

    x = np.arange(-3.0, 3.0, 0.3)

    y = (-weights[0] - weights[1] * x) / weights[2]

    axis.plot(x, y)

    plt.xlabel('x1')
    plt.ylabel('x2')

    plt.show()


def load_data_set():

    data_set = []

    labels = []

    fr = open("./sample/testSet.txt")

    for line in fr.readlines():

        vector = line.strip().split()

        data_set.append([1.0, float(vector[0]), float(vector[1])])

        labels.append(int(vector[-1]))

    return data_set, labels


def sigmoid(x):
    return 1.0 / (1.0 + np.power(np.e, -x))


def stochastic_grad_descent(data_set, labels, max_iterations=500):

    m, n = np.shape(data_set)

    weights = np.ones(n)

    for iter_time in range(max_iterations):

        data_indices = range(m)

        for row in range(m):

            alpha = 4 / (1.0 + iter_time + row) + 0.01

            random_index = int(random.uniform(0, len(data_indices)))

            result = sigmoid(np.sum(data_set[random_index] * weights))

            error_offset = result - labels[random_index]

            # delta = data_set[row] * error_offset

            delta = np.dot(data_set[random_index], error_offset)

            weights -= alpha * delta

            del data_indices[random_index]

    return weights


def grad_descent(data_set, labels):

    # data_mat is a m * n mat

    data_mat = np.matrix(data_set)

    # label_mat is a m * 1 mat

    labels_mat = np.matrix(labels).transpose()

    m, n = np.shape(data_set)

    alpha = 0.001

    max_iterations = 100

    weights = np.ones((n, 1))

    for index in range(max_iterations):

        x = np.dot(data_mat, weights)

        result = sigmoid(np.sum(x))

        # error_offset is a m * 1 mat

        error_mat = result - labels_mat

        # grad(weights) is data_mat, but why?

        weights -= alpha * np.dot(data_mat.transpose(), error_mat)

    return weights


def classify(vector, weights):

    result = sigmoid(np.dot(vector, weights))

    return 1.0 if result > 0.5 else 0.0


def test():

    training_set_fp = open("./sample/horseColicTraining.txt")

    test_set_fp = open("./sample/horseColicTest.txt")

    training_set = []
    labels = []

    lines = training_set_fp.readlines()

    for line in lines:

        current_line = line.strip().split('\t')

        vector = []

        for index in range(21):

            vector.append(float(current_line[index]))

        training_set.append(vector)

        labels.append(float(current_line[21]))

    weights = stochastic_grad_descent(training_set, labels)

    error_count, test_vector_count = 0.0, 0.0

    lines = test_set_fp.readlines()

    for line in lines:

        test_vector_count += 1.0

        current_line = line.strip().split('\t')

        vector = []

        for index in range(21):

            vector.append(float(current_line[index]))

        result = classify(vector, weights)

        if int(result) != int(current_line[21]):

            error_count += 1

    error_rate = (float(error_count) / test_vector_count)

    print "Error rate is %f" % error_rate

    return error_rate


def times_test(times):

    error_rate_sum = 0.0

    for time in range(times):

        error_rate_sum += test()

    average_error_rate = error_rate_sum / float(times)

    print "%d times average error rate is %f" % (times, average_error_rate)


if __name__ == '__main__':

    times_test(10)

