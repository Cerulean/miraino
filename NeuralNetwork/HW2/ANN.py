# -*- coding:utf-8 -*-


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import json

class NeuralNetwork:

    def __init__(self):

        self.layers = []
        self.parameters = []

    def add_layer(self, layer):

        if len(self.layers) > 0:

            layer.connect(self.layers[-1])

        else:

            layer.connect()

        self.layers.append(layer)

    def compute(self, x):

        results = np.array(x)

        for layer in self.layers:

            if layer.has_bais:

                results = np.append(results, np.array([1]))

            results = layer.compute(results)

        return results

    def predict(self, x):

        results = []

        for xx in x:

            results.append(self.compute(xx))

        return np.array(results)

    def train(self, training_set, labels, eta=0.3, threshold=None, max_iteration_times=None):

        training_set_size = len(training_set)

        index, iteration_times, batch_times = 0, 0, 1

        error = np.array([100.0] * training_set_size)

        while True:

            vector_input = training_set[index]

            label = labels[index]

            vector_output = self.compute(vector_input)

            vector_offset = vector_output - label

            index = (index + 1) % training_set_size

            iteration_times += 1

            error[index] = float(np.sqrt(np.dot(vector_offset, vector_offset)))

            if iteration_times % training_set_size == 0:

                batch_times += 1

            if threshold and np.all(error < threshold):
                break

            if max_iteration_times and iteration_times > max_iteration_times:
                break

            self.back_propagation(vector_offset)

            self.record_parameters(iteration_times)

            self.update(eta)

        self.save_parameters_to_json()
        self.save_parameters_to_csv()

    def back_propagation(self, vector_offset):

        for layer in self.layers[::-1]:

            layer.back_propagation(vector_offset)

    def update(self, eta):

        for layer in self.layers:

            layer.update(eta)

    def record_parameters(self, index):

        # Record weights and deltas and weights_deltas

        weights = []

        deltas = []

        weights_deltas = []

        for layer in self.layers:

            for neuron in layer.neurons:

                deltas.append("%.4f" % neuron.delta)

                weights_deltas.append([str("%.4f" % weights_delta) for weights_delta in neuron.weights_delta.tolist()])

                for weight in neuron.weights:

                    weights.append("%.4f" % weight)

        # Record output

        outputs = []

        for layer in self.layers:

            outputs.append([str("%.4f" % output) for output in layer.output])

        # Save to parameters

        parameter = {"weights":        weights,
                     "deltas":         deltas,
                     "weights_deltas": weights_deltas,
                     "outputs":        outputs}

        self.parameters.append(parameter)

    def save_parameters_to_json(self):

        fp = open("./HW2/ann.json", "w")

        json.dump(self.parameters, fp, indent=1)

    def save_parameters_to_csv(self):

        parameters = []

        for parameter in self.parameters:

            data = []

            data.extend(parameter["weights"])
            data.extend(parameter["outputs"])
            data.extend(parameter["deltas"])
            data.extend(parameter["weights_deltas"])

            parameters.append(data)

        parameters_frame = pd.DataFrame(parameters, columns=["L1-W(1-1)",
                                                             "L1-W(0-1)",
                                                             "L1-W(1-2)",
                                                             "L1-W(0-2)",
                                                             "L2-W(1-1)",
                                                             "L2-W(2-1)",
                                                             "L2-W(0-1)",
                                                             "L1-Z",
                                                             "L2-Y",
                                                             "L1-Delta-1",
                                                             "L1-Delta-2",
                                                             "L2-Delta",
                                                             "L1-Delta-W1",
                                                             "L1-Delta-W2",
                                                             "L2-Delta",
                                                             ])

        parameters_frame.to_csv("./HW2/ann.csv")

    @staticmethod
    def plot_spiral_scatter(a, b, theta_list):

        figure = plt.figure()

        axes = figure.add_subplot(111, polar=True)

        rho_list = [a + b * theta for theta in theta_list]

        axes.scatter(theta_list, rho_list, s=10, c='blue')

        plt.show()


class Layer:

    def __init__(self,
                 number_of_neurons,
                 initial_weights_list=None,
                 input_size=None,
                 activation_name=None,
                 has_bais=True):

        self.number_of_neurons = number_of_neurons

        self.neurons = []

        self.next_layer = None

        self.input_size = input_size

        self.activation_name = activation_name

        self.initial_weights_list = initial_weights_list

        self.has_bais = has_bais

        self.output = None

    def connect(self, last_layer=None):

        if last_layer is not None:

            self.input_size = last_layer.get_output_size()

            last_layer.set_next_layer(self)

        for index in np.arange(0, self.number_of_neurons):

            initial_weights = None

            if self.initial_weights_list:

                initial_weights = self.initial_weights_list[index]

            self.neurons.append(Neuron(self,
                                       index,
                                       initial_weights=initial_weights,
                                       activation_name=self.activation_name))

    def compute(self, x):

        output = []

        for neuron in self.neurons:

            output.append(neuron.compute(x))

        self.output = output

        return self.output

    def back_propagation(self, vector_offset):

        for neuron in self.neurons:

            neuron.back_propagation(vector_offset)

    def update(self, eta):

        for neuron in self.neurons:

            neuron.update(eta)

    def set_next_layer(self, layer):

        self.next_layer = layer

    def get_output_size(self):

        return len(self.neurons) + 1 if self.has_bais else 0


class Neuron:

    def __init__(self, layer, index, initial_weights=None, activation_name=None):

        self.x = None

        self.weighted_x = None

        self.index = index

        self.layer = layer

        self.weights = np.array(initial_weights) if initial_weights else np.array(np.random.rand(self.layer.input_size))

        self.delta = 0.0

        self.weights_delta = np.array(np.zeros(self.layer.input_size))

        if activation_name == "hyperbolic":

            self.activation = Neuron.hyperbolic

            self.gradient_activation = Neuron.gradient_hyperbolic

        elif activation_name == "sigmoid":

            self.activation = Neuron.sigmoid

            self.gradient_activation = Neuron.gradient_sigmoid

        else:

            self.activation = Neuron.hold

            self.gradient_activation = Neuron.gradient_hold

    def compute(self, x):

        self.x = x

        self.weighted_x = np.dot(self.x, self.weights)

        return self.activation(self.weighted_x)

    def back_propagation(self, vector_offset):

        # For grad, grad(w_l) = δ_(l) * x_(l-1)

        if self.layer.next_layer is not None:

            # For Hidden Layer, δ_(l) = ∑ δ_(l+1) * grad(s_(l+1)) * grad(x_(l))

            sum_of_terms = 0.0

            for neuron in self.layer.next_layer.neurons:

                sum_of_terms += neuron.delta * neuron.weights[self.index]

        else:

            # For Output Layer, δ_(L) = (y - s_(L)) * x_(L)

            sum_of_terms = vector_offset[self.index]

        self.delta = self.gradient_activation(self.weighted_x) * sum_of_terms

    def update(self, eta):

        # grad(w_(l)) = δ_(l) * x_(l-1)

        self.weights_delta = eta * self.delta * np.array(self.x)

        self.weights -= self.weights_delta

    @staticmethod
    def hold(x):
        return x

    @staticmethod
    def gradient_hold(x):
        return x - x + 1

    @staticmethod
    def sigmoid(x):
        return 1.0 / (1.0 + np.power(np.e, -x))

    @staticmethod
    def gradient_sigmoid(x):
        return Neuron.sigmoid(x) * (1 - Neuron.sigmoid(x))

    @staticmethod
    def hyperbolic(x):
        return (np.power(np.e, x) - np.power(np.e, -x)) / (np.power(np.e, x) + np.power(np.e, -x))

    @staticmethod
    def gradient_hyperbolic(x):
        return 1 - np.power(Neuron.hyperbolic(x), 2)


if __name__ == "__main__":

    network = NeuralNetwork()

    # network.add_layer(Layer(2,
    #                         has_bais=True,
    #                         input_size=2,
    #                         activation_name="hyperbolic",
    #                         initial_weights_list=[[0.3, 0.0], [-0.3, 0.0]]))
    #
    # network.add_layer(Layer(1,
    #                         activation_name="hold",
    #                         initial_weights_list=[[-0.1, 0.1, 0.0]]))

    network.add_layer(Layer(5,
                            has_bais=True,
                            input_size=4,
                            activation_name="hyperbolic"))

    network.add_layer(Layer(3,
                            has_bais=True,
                            activation_name="hyperbolic"))

    network.add_layer(Layer(1,
                            activation_name="hold"))



    batch_of_x = np.array([[0.8]])
    batch_of_y = np.array([[0.72]])

    network.train(batch_of_x, batch_of_y, eta=0.3, threshold=None, max_iteration_times=301)

