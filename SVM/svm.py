# -*- coding:utf-8 -*-

import os

import numpy as np
import random


def load_images(file_path):

    files = os.listdir(file_path)

    rows = len(files)

    data_set = []

    labels = []

    for index in range(rows):

        filename = files[index]

        path = file_path + '/' + filename

        data_set.append(image_to_matrix(path))

        label = int(filename.split('.')[0].split('_')[0])

        labels.append(-1 if label == 9 else 1)

    return data_set, labels


def image_to_matrix(filename):

    vector = np.zeros((1, 1024))

    image = open(filename)

    for i in range(32):

        line = image.readline()

        for j in range(32):

            vector[0, 32 * i + j] = int(line[j])

    vector = vector.tolist()[0]

    return vector


def load_data_set(filename):

    data_set, labels = [], []

    fp = open(filename)

    for line in fp.readlines():

        vector = line.strip().split('\t')

        data_set.append([float(vector[0]), float(vector[1])])

        labels.append(float(vector[-1]))

    return data_set, labels


def select_random_index(index_of_alpha, alpha_count):

    selected_index = index_of_alpha

    while selected_index == index_of_alpha:

        selected_index = int(random.uniform(0, alpha_count))

    return selected_index


def clip_alpha(index_of_alpha, upper_bound, lower_bound):

    index_of_alpha = upper_bound if index_of_alpha > upper_bound else index_of_alpha
    index_of_alpha = lower_bound if index_of_alpha < lower_bound else index_of_alpha

    return index_of_alpha


def sample_smo(training_set, labels, C, tolerate, max_iterations):

    training_mat = np.mat(training_set)

    labels_mat = np.mat(labels).transpose()

    b = 0

    rows, cols = training_mat.shape

    alphas = np.mat(np.zeros((rows, 1)))

    iteration = 0

    while iteration < max_iterations:

        is_pair_alpha_changed = False

        for index in range(rows):

            # Important Fact: w = ∑α_n * y_n * z_n, z_n = x_n if there is no kernel

            # alpha is m * 1, labels_mat is m * 1, training_mat is m * n

            # (alpha * labels_mat.T).T is 1 * m

            # w is 1 * n

            # x is 1 * n

            w = (np.multiply(alphas, labels_mat).T * training_mat)

            x = training_mat[index, :]

            y = labels[index]

            output_of_index = float(w * x.T) + b

            delta = output_of_index - float(y)

            # Check KKT condition, there is three important fact:

            # 1. Primal Feasible: y * (w * x.T + b) >= 1

            # 2. Dual Feasible: α >= 0

            # 3. Dual-Inner Primal ∑α_n * y_n = 0, w = ∑α_n * y_n * z_n, z_n = x_n

            # 4. Complementary Slackness: α * (1 - (w * x.T + b)) = 0

            if y * delta < -tolerate and alphas[index] < C or y * delta > tolerate and alphas[index] > 0:

                selected_index = select_random_index(index, rows)

                x_s = training_mat[selected_index, :]

                y_s = labels[selected_index]

                output_of_selected_index = float(w * x_s.T) + b

                delta_s = output_of_selected_index - float(y_s)

                old_alpha_of_index = alphas[index].copy()

                old_alpha_of_selected_index = alphas[selected_index].copy()

                if y != y_s:

                    lower_bound_of_alpha = max(0, alphas[selected_index] - alphas[index])
                    upper_bound_of_alpha = min(C, C + alphas[selected_index] - alphas[index])

                else:

                    lower_bound_of_alpha = max(0, alphas[selected_index] + alphas[index] - C)
                    upper_bound_of_alpha = min(C, alphas[selected_index] + alphas[index])

                if lower_bound_of_alpha == upper_bound_of_alpha:

                    continue

                eta = 2.0 * x * x_s.T - x * x.T - x_s * x_s.T

                if eta >= 0:

                    continue

                alphas[selected_index] -= y_s * (delta - delta_s) / eta

                alphas[selected_index] = clip_alpha(alphas[selected_index], upper_bound_of_alpha, lower_bound_of_alpha)

                if abs(alphas[selected_index] - old_alpha_of_selected_index) < 0.00001:

                    continue

                alphas[index] += y_s * y * (old_alpha_of_selected_index - alphas[selected_index])

                b1 = b - delta - y * (alphas[index] - old_alpha_of_index) * x * x.T - \
                     y_s * (alphas[selected_index] - old_alpha_of_selected_index) * x * x_s.T

                b2 = b - delta_s - y * (alphas[index] - old_alpha_of_index) * x * x_s.T - \
                     y_s * (alphas[selected_index] - old_alpha_of_selected_index) * x_s * x_s.T

                if 0 < alphas[index] < C:
                    b = b1
                elif 0 < alphas[selected_index] < C:
                    b = b2

                is_pair_alpha_changed = True

                print 'iteration is %d, index is %d, pair alpha changed is %d' % (iteration,
                                                                                  index,
                                                                                  is_pair_alpha_changed)

        if is_pair_alpha_changed is False:

            iteration += 1

        else:

            iteration = 0

        print "iteration is %d" % iteration

    return b, alphas


class SVM(object):

    def __init__(self, training_set, labels, test_set, test_labels, c, tolerate, max_iterations, kernel_info=['linear', 1.3]):

        self.origin_training_set = np.mat(training_set)

        self.origin_test_set = np.mat(test_set)

        self.training_set = self.kernel_trick(self.origin_training_set, kernel_info)

        self.test_set = self.kernel_trick(self.origin_test_set, kernel_info)

        self.labels = np.mat(labels).transpose()

        self.test_labels = np.mat(test_labels).transpose()

        self.C = c

        self.tolerate = tolerate

        self.max_iterations = max_iterations

        self.training_set_rows = self.training_set.shape[0]
        self.training_set_cols = self.training_set.shape[1]

        self.alphas = np.mat(np.zeros((self.training_set_rows, 1)))

        self.b = 0

        self.w = np.zeros((1, self.training_set_rows))

        self.error_cache = np.mat(np.zeros((self.training_set_rows, 2)))

    def calculate_error(self, index):

        # alpha is m * 1 mat, labels is m * 1 mat , training_set is m * n mat

        # alpha * labels.T is m * 1 mat

        # w is 1 * n mat

        # x is 1 * n mat

        w = np.multiply(self.alphas, self.labels).T * self.training_set

        x = self.training_set[index]

        y = self.labels[index]

        output_of_index = w * x.T + self.b

        error_of_index = output_of_index - float(y)

        return error_of_index

    def select_heuristic_index(self, index_of_alpha, error_of_index):

        index_of_max_delta, max_delta, error_of_selected_index = -1, 0, 0

        self.error_cache[index_of_alpha] = [1, error_of_index]

        valid_error_cache_indices = np.nonzero(self.error_cache[:, 0].A)[0]

        if len(valid_error_cache_indices) > 1:

            for cache_index in valid_error_cache_indices:

                if cache_index == index_of_alpha:
                    continue

                error_of_cache_index = self.calculate_error(cache_index)

                delta = abs(error_of_index - error_of_cache_index)

                if delta > max_delta:

                    index_of_max_delta = cache_index

                    max_delta = delta

                    error_of_selected_index = error_of_cache_index

            return index_of_max_delta, error_of_selected_index

        else:

            selected_index = select_random_index(index_of_alpha, self.training_set_rows)

            error_of_selected_index = self.calculate_error(selected_index)

            return selected_index, error_of_selected_index

    def update_error(self, index):

        error_of_index = self.calculate_error(index)

        self.error_cache[index] = [1, error_of_index]

    def inner_loop(self, index):

        error_of_index = self.calculate_error(index)

        y = self.labels[index]

        if y * error_of_index < -self.tolerate and self.alphas[index] < self.C or \
           y * error_of_index >  self.tolerate and self.alphas[index] > 0:

            selected_index, error_of_selected_index = self.select_heuristic_index(index, error_of_index)

            alpha_of_index = self.alphas[index]

            alpha_of_selected_index = self.alphas[selected_index]

            old_alpha_of_index = self.alphas[index].copy()

            old_alpha_of_selected_index = self.alphas[selected_index].copy()

            y_selected = self.labels[selected_index]

            if y_selected != y:

                lower_bound_of_alpha = max(0, alpha_of_selected_index - alpha_of_index)
                upper_bound_of_alpha = min(self.C, self.C + alpha_of_selected_index - alpha_of_index)

            else:

                lower_bound_of_alpha = max(0, alpha_of_selected_index + alpha_of_index - self.C)
                upper_bound_of_alpha = min(self.C, alpha_of_selected_index + alpha_of_index)

            if lower_bound_of_alpha == upper_bound_of_alpha:

                return 0

            x = self.training_set[index]

            x_selected = self.training_set[selected_index]

            eta = 2 * x * x_selected.T - x * x.T - x_selected * x_selected.T

            if eta >= 0:

                return 0

            self.alphas[selected_index] -= y_selected * (error_of_index - error_of_selected_index) / eta
            self.alphas[selected_index] = clip_alpha(alpha_of_selected_index,
                                                     upper_bound_of_alpha,
                                                     lower_bound_of_alpha)

            self.update_error(selected_index)

            if abs(alpha_of_selected_index - old_alpha_of_selected_index) < 0.00001:

                return 0

            alpha_of_index += y_selected * y * (old_alpha_of_selected_index - alpha_of_selected_index)

            self.update_error(index)

            b1 = self.b - error_of_index - y * (alpha_of_index - old_alpha_of_index) * x * x.T - \
                y_selected * (alpha_of_selected_index - old_alpha_of_selected_index) * x * x_selected.T

            b2 = self.b - error_of_selected_index - y * (alpha_of_index - old_alpha_of_index) * x * x_selected.T - \
                y_selected * (alpha_of_selected_index - old_alpha_of_selected_index) * x_selected * x_selected.T

            if 0 < alpha_of_index < self.C:

                self.b = b1

            elif 0 < alpha_of_selected_index < self.C:

                self.b = b2

            else:

                self.b = (b1 + b2) / 2.0

            return 1

        else:

            return 0

    def smo(self):

        iteration = 0

        is_entire_set, alpha_pairs_changed_count = True, 0

        while iteration < self.max_iterations and alpha_pairs_changed_count > 0 or is_entire_set:

            alpha_pairs_changed_count = 0

            if is_entire_set:

                for index in range(self.training_set_rows):

                    alpha_pairs_changed_count += self.inner_loop(index)

                iteration += 1

            else:

                indices_of_alphas_not_on_bounds = np.nonzero((self.alphas.A > 0) * (self.alphas.A < self.C))[0]

                for index in indices_of_alphas_not_on_bounds:

                    alpha_pairs_changed_count += self.inner_loop(index)

                iteration += 1

            if is_entire_set:

                is_entire_set = False

            elif alpha_pairs_changed_count == 0:

                is_entire_set = True

        for index in range(self.training_set_rows):

            vector = self.training_set[index].T

            self.w += np.multiply(self.alphas[index] * self.labels[index], vector).T

        return self.w, self.b, self.alphas

    @staticmethod
    def kernel_trick(data_set, kernel_info):

        kernel = kernel_info[0]

        Z = []

        if kernel == 'linear':

            for vector in data_set:

                # vector is 1 * n, training_set.T is n * m

                z = vector * data_set.T

                Z.extend(z.A)

        elif kernel == 'rbf':

            rbf_delta = kernel_info[1]

            for vector in data_set:

                z = []

                for index in range(len(data_set)):

                    sub_vector = data_set[index]

                    delta = vector - sub_vector

                    scalar_product_of_delta = (delta * delta.T).A[0]

                    z.extend(scalar_product_of_delta)

                z = np.array(z)

                z = np.exp(z / (-1 * rbf_delta ** 2))

                Z.append(z)

        else:

            raise NameError("Kernel not recognized.")

        Z = np.mat(Z)

        return Z

    def test(self):

        w, b, alphas = svm.smo()

        print "Number of Support Vectors is %d" % len(np.nonzero(alphas.A > 0)[0])

        self.calculate_error_ratio("Training", self.training_set, self.labels, w, b)

        self.calculate_error_ratio("Test", self.test_set, self.test_labels, w, b)

    @staticmethod
    def calculate_error_ratio(tag, data_set, labels, w, b):

        error_count = 0.0

        label_count = len(labels)

        for index in range(label_count):

            vector = data_set[index]

            result = w * vector.T + b

            label = labels[index]

            if np.sign(result) != np.sign(label):

                error_count += 1

        error_ratio = error_count / float(label_count)

        print "%s error ratio is %f" % (tag, error_ratio)


if __name__ == "__main__":

    _training_set, _training_labels = load_data_set("./sample/testSetRBF.txt")
    #
    # _test_set, _test_labels = load_data_set("./sample/testSetRBF2.txt")

    _training_set, _training_labels = load_images("./sample/trainingDigits")

    _test_set, _test_labels = load_images("./sample/testDigits")

    svm = SVM(_training_set, _training_labels, _test_set, _test_labels, 100, 0.001, 1000, ['rbf', 1.0])

    svm.test()
