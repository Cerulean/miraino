# -*- coding:utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

from Base.base import BaseObject


class LinearRegression(BaseObject):

    def __init__(self, training_set, training_labels):

        super(LinearRegression, self).__init__(training_set, training_labels)

        self.w = None

    def train(self, option='linear'):

        if option == 'linear':

            self.linear_train()

    def linear_train(self):

        # The w^ is (x.T * x)^(-1) * x.T * y
        # The w^ is n * 1 mat
        # The x  is m * n mat

        x_squared = self.training_set.T * self.training_set

        if np.linalg.det(x_squared) == 0.0:

            print "x_squared is singular and has no inverse."

            return

        self.w = x_squared.I * self.training_set.T * self.training_labels

        return self.w

    def predict(self, vector, option='linear', k=1.0):

        result = None

        if option == 'linear':

            result = vector * self.w

        elif option == 'LWLR':

            result = self.locally_weighted_predict(vector, k)

        return float(result)

    def locally_weighted_predict(self, vector, k):

        rows = self.training_set.shape[0]

        weights_mat = np.mat(np.eye(rows))

        for row in range(rows):

            offset_mat = vector - self.training_set[row, :]

            weights_mat[row, row] = np.exp(abs(offset_mat * offset_mat.T) / -2.0 * (k ** 2))

            # The w^  is (x.T * W * x)^(-1) * x.T * W * y
            # The w^  is n * 1 mat
            # The x.T is n * m mat
            # The W   is m * m mat

        x_squared = self.training_set.T * (weights_mat * self.training_set)

        if np.linalg.det(x_squared) == 0.0:

            print "x_squared is singular and has no inverse."

            return

        w = x_squared.I * (self.training_set.T * (weights_mat * self.training_labels))

        result = vector * w

        return result

    @staticmethod
    def plot_scatter(data_set, label_set, result_set):

        figure = plt.figure()

        axes = figure.add_subplot(111)
        axes.scatter(data_set[:, 1].flatten().A[0],
                     label_set[:, 0].flatten().A[0],
                     s=5,
                     c='red')

        # x is m * n mat
        # w is n * 1 mat
        # y is m * 1 mat

        x = data_set.copy()
        x.sort(0)

        y = result_set
        y.sort(0)

        axes.plot(x[:, 1].flatten().A[0], y[:, 0].flatten().A[0])

        plt.show()

if __name__ == '__main__':

    training_set, training_labels = LinearRegression.load_data_set('./sample/ex0.txt')

    linearRegression = LinearRegression(training_set, training_labels)

    result_set = []

    for vector in linearRegression.training_set:

        result = linearRegression.predict(vector, option='LWLR', k=100)

        result_set.append(result)

    result_set = np.mat(result_set).T

    linearRegression.plot_scatter(linearRegression.training_set, linearRegression.training_labels, result_set)

