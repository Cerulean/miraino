# -*- coding:utf-8 -*-

import operator
import treePloter
import pickle
import json
import os

from math import log


def calculate_shannon_entropy(data_set):

    entries_count = len(data_set)

    labels = {}

    for vector in data_set:

        current_label = vector[-1]

        if current_label not in labels.keys():
            labels[current_label] = 0

        labels[current_label] += 1

    shannon_entropy = 0.0

    for key in labels:

        probability = float(labels[key]) / entries_count

        shannon_entropy -= probability * log(probability, 2)

    return shannon_entropy


def create_data_set():

    data_set = [['50%', 5000, 'BAD'],
                ['10%', 9999, 'GOOD'],
                ['10%', 6000, 'BAD'],
                ['40%', 6000, 'GOOD'],
                ['30%', 6000, 'BAD'],
                ['30%', 7000, 'GOOD'],
                ['99%', 4500, 'GOOD'],
                ['10%', 4500, 'BAD'],
                ['10%', 7777, 'GOOD']]

    tags = ['Bonus', 'Salary']

    return data_set, tags


def split_data_set(data_set, index, value):

    new_data_set = []

    for vector in data_set:

        if vector[index] == value:

            split_vector = vector[:index]
            split_vector.extend(vector[index + 1:])

            new_data_set.append(split_vector)

    return new_data_set


def get_index_of_best_split_feature(data_set):

    features_count = len(data_set[0]) - 1

    base_entropy = calculate_shannon_entropy(data_set)

    base_info_gain = 0.0

    best_feature_index = -1

    for index in range(features_count):

        feature_values = [vector[index] for vector in data_set]

        unique_feature_values = set(feature_values)

        new_entropy = 0.0

        for value in unique_feature_values:

            sub_data_set = split_data_set(data_set, index, value)

            probability = len(sub_data_set) / float(len(data_set))

            new_entropy += probability * calculate_shannon_entropy(sub_data_set)

        info_gain = base_entropy - new_entropy

        if info_gain > base_info_gain:
            base_info_gain = info_gain
            best_feature_index = index

    return best_feature_index


def decide_label(label_results):

    label_count_dic = {}

    for label in label_results:

        if label not in label_count_dic.keys():
            label_count_dic[label] = 0

        label_count_dic[label] += 1

    sorted_label_count_list = sorted(label_count_dic.iteritems(), key=operator.itemgetter(1), reverse=True)

    return sorted_label_count_list[0][0]


def create_decision_tree(data_set, tags):

    labels = [vector[-1] for vector in data_set]

    if labels.count(labels[0]) == len(labels):
        return labels[0]

    if len(data_set[0]) == 1:
        return decide_label(labels)

    best_split_feature_index = get_index_of_best_split_feature(data_set)

    best_split_feature_tag = tags[best_split_feature_index]

    decision_tree = {best_split_feature_tag: {}}

    del(tags[best_split_feature_index])

    best_split_features = [vector[best_split_feature_index] for vector in data_set]

    best_split_features_set = set(best_split_features)

    for best_split_feature in best_split_features_set:

        sub_tags = tags[:]

        sub_data_set = split_data_set(data_set, best_split_feature_index, best_split_feature)

        sub_decision_tree = create_decision_tree(sub_data_set, sub_tags)

        decision_tree[best_split_feature_tag][best_split_feature] = sub_decision_tree

    return decision_tree


def classify(decision_tree, tags, x):

    root_tag = decision_tree.keys()[0]

    sub_tree = decision_tree[root_tag]

    tag_index = tags.index(root_tag)

    label = None

    for key in sub_tree:

        feature_value = x[tag_index]

        if key == feature_value:

            if isinstance(sub_tree[key], dict):

                label = classify(sub_tree[key], tags, x)

            else:

                label = sub_tree[key]

    return label


def save_decision_tree(decision_tree, file_name):

    tree_file = open(file_name, 'w')

    json.dump(decision_tree, tree_file)

    tree_file.close()


def load_decision_tree(file_name):

    tree_file = open(file_name)

    return json.load(tree_file)


if __name__ == '__main__':

    _data_set, _tags = create_data_set()

    tree = load_decision_tree("./trees/salaryTree")

    treePloter.create_plot(tree)

    print classify(tree, _tags, ["0%", "10000"])

    # treePloter.create_plot(tree)
