# -*- coding:utf-8 -*-


import numpy as np

import random
import re


def load_training_set():

    training_set = [['my', 'dog', 'has', 'flea', 'problems', 'help', 'please'],
                    ['maybe', 'not', 'take', 'him', 'to', 'dog', 'park', 'stupid'],
                    ['my', 'dalmation', 'is', 'so', 'cute', 'I', 'love', 'him'],
                    ['stop', 'posting', 'stupid', 'worthless', 'garbage'],
                    ['mr', 'licks', 'ate', 'my', 'steak', 'how', 'to', 'stop', 'him'],
                    ['quit', 'buying', 'worthless', 'dog', 'food', 'stupid']]

    vocabulary_list = generate_vocabulary_list(training_set)

    vocabulary_vectors = [convert_words_to_vector(vocabulary_list, vector) for vector in training_set]

    labels = [0,
              1,
              0,
              1,
              0,
              1]

    return vocabulary_vectors, labels, vocabulary_list


def generate_vocabulary_list(training_set):

    new_traning_set = set([])

    for document in training_set:

        new_traning_set = new_traning_set | set(document)

    return list(new_traning_set)


def convert_words_to_vector(vocabulary_list, input_data_set):

    vector = [0] * len(vocabulary_list)

    for word in input_data_set:

        if word in vocabulary_list:

            vector[vocabulary_list.index(word)] += 1

        else:

            print "Word: %s is not in the vocabulary list" % word

    return vector


def parse_str(string):

    tokens = re.split('\w*', string)

    return [token.lower() for token in tokens]


def train(training_set, labels):

    training_set_size = len(training_set)

    vector_size = len(training_set[0])

    bad_probability = np.sum(labels) / float(len(labels))

    # For performance, we use ones(vector_size) to replace zeros(vector_size)

    vector_of_every_words_sum_bad = np.ones(vector_size)

    vector_of_every_words_sum_good = np.ones(vector_size)

    # For performance, we use 2.0 to replace 0.0

    word_vectors_sum_bad = 2.0

    word_vectors_sum_good = 2.0

    for index in range(training_set_size):

        if labels[index] == 1:
            
            vector_of_every_words_sum_bad += training_set[index]

            word_vectors_sum_bad += np.sum(training_set[index])

        if labels[index] == 0:

            vector_of_every_words_sum_good += training_set[index]

            word_vectors_sum_good += np.sum(training_set[index])

    # For accuracy, we use log to replace direct divide into ln(x)

    vector_of_every_word_and_bad_probability = np.log(vector_of_every_words_sum_bad / word_vectors_sum_bad)

    vector_of_every_word_and_good_probability = np.log(vector_of_every_words_sum_good / word_vectors_sum_good)

    return vector_of_every_word_and_bad_probability, vector_of_every_word_and_good_probability, bad_probability


def classify(vector, p0, p1, p):

    # For accuracy, we use ln(x) to raplace origin bad probability

    aye = np.sum(vector * p0) + np.log(p)

    nay = np.sum(vector * p1) + np.log(1 - p)

    return 1 if aye > nay else 0


def test():

    vectors, labels, vocabularies = [], [], []

    for index in range(1, 26):

        # Bad cases

        words = parse_str(open("./email/spam/%d.txt" % index).read())

        vectors.append(words)

        labels.append(1)

        vocabularies.extend(words)

        # Good cases

        words = parse_str(open("./email/ham/%d.txt" % index).read())

        vectors.append(words)

        labels.append(0)

        vocabularies.extend(words)

    all_indices = range(50)

    test_indices = []

    # Divide training index and testing index

    for index in range(10):

        random_index = int(random.uniform(0, len(all_indices)))

        test_indices.append(all_indices[random_index])

        del all_indices[random_index]

    training_indices = all_indices

    training_set = []

    training_labels = []

    vocabulary_list = generate_vocabulary_list(vectors)

    for index in training_indices:

        training_set.append(convert_words_to_vector(vocabulary_list, vectors[index]))

        training_labels.append(labels[index])

    # p0 = word_appear_times_and_bad_probability

    # p1 = word_appear_times_and_good_probability

    # p = bad_probability

    p0, p1, p = train(training_set, training_labels)

    error_count = 0

    for index in test_indices:

        vector = convert_words_to_vector(vocabulary_list, vectors[index])

        label = labels[index]

        result = classify(vector, p0, p1, p)

        if label != result:

            error_count += 1

        print "Classification error at index of %d" % index

    print "Error rate is %f" % (float(error_count) / len(test_indices))


if __name__ == '__main__':

    test()