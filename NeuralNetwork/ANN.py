# -*- coding:utf-8 -*-


import matplotlib.pyplot as plt
import numpy as np
import json

from matplotlib.colors import ListedColormap


class NeuralNetwork(object):

    def __init__(self, training_set=None, training_labels=None, test_set=None, test_labels=None, eta=0.3):

        self.training_set = training_set

        self.training_labels = training_labels

        self.test_set = test_set

        self.test_labels = test_labels

        self.eta = eta

        self.layers = []

        self.mean_errors = []

        self.in_sample_errors = []

        self.out_of_sample_errors = []

    def add_layer(self, layer):

        if len(self.layers) > 0:

            layer.connect(self.layers[-1])

        else:

            layer.connect()

        self.layers.append(layer)

    def compute(self, vector):

        result = vector

        for layer in self.layers:

            result = layer.compute(result)

        return result

    def back_propagation(self, diff):

        for layer in self.layers[::-1]:

            layer.back_propagation(diff)

        for layer in self.layers:

            layer.update(self.eta)

    def save(self):

        layer_info_list = []

        for layer in self.layers:

            neuron_info_list = []

            for neuron in layer.neurons:
                neuron_info_list.append(neuron.weights.tolist())

            layer_info = {"neuron_info_list": neuron_info_list,
                          "neuron_count": layer.neuron_count,
                          "vector_cols": layer.vector_cols,
                          "activation": layer.activation}

            layer_info_list.append(layer_info)

        network_info = {"eta": self.eta,
                        "mean_errors": self.mean_errors,
                        "layer_info_list": layer_info_list,
                        "in_sample_errors": self.in_sample_errors,
                        "out_of_sample_errors": self.out_of_sample_errors}

        fr = open("./HW3/network_info.json", 'w')

        json.dump(network_info, fr, indent=True)

        fr.close()

    def load(self):

        fr = open("./HW3/network_info.json", 'r')

        network_info = json.load(fr)

        eta = network_info["eta"]

        self.out_of_sample_errors = network_info["out_of_sample_errors"]

        self.in_sample_errors = network_info["in_sample_errors"]

        self.mean_errors = network_info["mean_errors"]

        self.eta = eta

        layer_info_list = network_info["layer_info_list"]

        for layer_info in layer_info_list:

            neuron_info_list = []

            for neuron_info in layer_info["neuron_info_list"]:
                neuron_info_list.append(np.array(neuron_info))

            neuron_count = layer_info["neuron_count"]

            vector_cols = layer_info["vector_cols"]

            activation = layer_info["activation"]

            layer = Layer(neuron_count, vector_cols=vector_cols, weights_list=neuron_info_list, activation=activation)

            self.add_layer(layer)

    def train(self, mean_error_threshold=0.05, in_sample_error_threshold=0.1, batch_size=25, max_epoch=300):

        rows = self.training_set.shape[0]

        error_arr = np.array([100.0] * rows)

        epoch = 0

        indices = range(rows)

        # np.random.shuffle(indices)

        # batch_count = int(np.ceil(float(rows) / batch_size))
        #
        # batch_indices_list = []
        #
        # for batch_index in range(batch_count):
        #     batch_indices_list.append(indices[batch_index * batch_size:(batch_index + 1) * batch_size])

        while True:

            # for batch_indices in batch_indices_list:

            for index in indices:

                result = self.compute(self.training_set[index])

                diff = self.training_labels[index] - result

                error_arr[index] = float(np.sqrt(diff * diff))

                self.back_propagation(diff)

            epoch += 1

            # test_info_tuple = self.test(self.test_set, self.test_labels)

            # out_of_sample_error_ratio = test_info_tuple[-1]

            mean_error = np.mean(error_arr)

            if epoch % 10 == 0:

                self.mean_errors.append(mean_error)

            print "Epoch is %d, mean error is: %f" % (epoch, mean_error)

            #
            # in_sample_error = self.test(self.training_set, self.training_labels)[0]
            #
            # self.in_sample_errors.append(in_sample_error)
            #
            # out_of_sample_error = self.test(self.training_set, self.training_labels)[0]
            #
            # self.out_of_sample_errors.append(out_of_sample_error)

            # print "Epoch is %d, mean error is %f, E_in is %f, E_out is %f" % (epoch,
            #                                                                   mean_error,
            #                                                                   in_sample_error,
            #                                                                   out_of_sample_error)

            if mean_error <= mean_error_threshold:

                print "Mean error reach threshold, break."

                break

            # if in_sample_error <= in_sample_error_threshold:
            #
            #     print "E_in reach threshold, break."
            #
            #     break

            if epoch == max_epoch:

                print "Epoch reach threshold, break."

                break

    def classify(self, vector):

        result = float(self.compute(vector))

        result = 1.0 if result >= 0.5 else 0.0

        return result

    def test(self, test_set, test_labels, need_results=False):

        data_count = len(test_set)

        error_count = 0.0

        results = []

        for index in range(data_count):

            result = self.classify(test_set[index])

            if result != test_labels[index]:

                error_count += 1

            if need_results:

                results.append(result)

        error_ratio = error_count / data_count

        return error_ratio, results

    @staticmethod
    def generate_data(use_sin=True):

        NeuralNetwork.generate_spiral_data(1.0, "./HW3/positive_train.json", 0, 1.0, use_sin=use_sin)
        NeuralNetwork.generate_spiral_data(1.0, "./HW3/positive_test.json", 0.5, 1.0, use_sin=use_sin)

        NeuralNetwork.generate_spiral_data(0.0, "./HW3/negative_train.json", 0, 1.0, use_sin=use_sin)
        NeuralNetwork.generate_spiral_data(0.0, "./HW3/negative_test.json", 0.5, 1.0, use_sin=use_sin)

    @staticmethod
    def generate_spiral_data(label, filename, offset, radius, use_sin=True):

        data_set = []

        data_count = 80

        symbol = label if label == 1.0 else -1.0

        for index in range(data_count):

            angel_of_index = (index + offset) * np.pi / 25.0

            radius_of_index = radius * (data_count - index) / data_count

            x = symbol * -radius_of_index * np.sin(angel_of_index)
            y = symbol * radius_of_index * np.cos(angel_of_index)

            vector = [x, y]

            if use_sin:
                vector.append(np.sin(x))
                vector.append(np.sin(y))

            vector.append(label)

            data_set.append(vector)

        fr = open(filename, "w")

        json.dump(data_set, fr, indent=True)

        fr.close()

    @staticmethod
    def load_spiral_data():

        positive_training_set, positive_training_labels = NeuralNetwork.load_data("./HW3/positive_train.json")

        negative_training_set, negative_training_labels = NeuralNetwork.load_data("./HW3/negative_train.json")

        positive_test_set, positive_test_labels = NeuralNetwork.load_data("./HW3/positive_test.json")

        negative_test_set, negative_test_labels = NeuralNetwork.load_data("./HW3/negative_test.json")

        training_set = []
        training_set.extend(positive_training_set)
        training_set.extend(negative_training_set)

        training_labels = []
        training_labels.extend(positive_training_labels)
        training_labels.extend(negative_training_labels)

        test_set = []
        test_set.extend(positive_test_set)
        test_set.extend(negative_test_set)

        test_labels = []
        test_labels.extend(positive_test_labels)
        test_labels.extend(negative_test_labels)

        return_tuples = (np.array(training_set),
                         np.mat(training_labels).T.A,
                         np.mat(test_set),
                         np.mat(test_labels).T,
                         np.mat(positive_training_set),
                         np.mat(negative_training_set),
                         np.mat(positive_test_set),
                         np.mat(negative_test_set))

        return return_tuples

    @staticmethod
    def load_data(filename):

        fr = open(filename, 'r')

        data = json.load(fr)

        fr.close()

        data_set = []

        labels = []

        for vector in data:

            data_set.append(vector[:-1])

            labels.append(vector[-1])

        return data_set, labels

    def plot_spiral_scatter_and_error_curve(self,
                                            positive_training_set,
                                            negative_training_set,
                                            positive_test_set,
                                            negative_test_set):

        try:

            fr = open('./HW3/plot_info.json', 'r')

            info = json.load(fr)

            x_set = np.array(info["x_set"])
            y_set = np.array(info["y_set"])
            z_set = info["z_set"]

        except IOError:

            fr = open('./HW3/plot_info.json', 'w')

            x_set, y_set = np.meshgrid(np.arange(-1.2, 1.2, 0.01),
                                       np.arange(-1.2, 1.2, 0.01))

            z_set = []

            for index in range(len(x_set.ravel())):

                x = x_set.ravel()[index]
                y = y_set.ravel()[index]

                z = self.classify([x, y])

                z_set.append(z)

            info = {"x_set": x_set.tolist(),
                    "y_set": y_set.tolist(),
                    "z_set": z_set}

            json.dump(info, fr, indent=True)

        z_set = np.array(z_set).reshape(x_set.shape)

        figure = plt.figure("Two Spiral Problem")

        # Decision Boundary

        axes = figure.add_subplot(121)
        axes.set_title("Decision Boundary")

        camp = ListedColormap(['#8B008B', '#66CCFF'])

        plt.pcolormesh(x_set, y_set, z_set, cmap=camp)

        axes.scatter(positive_training_set[0], positive_training_set[1], s=5, c='red')
        axes.scatter(negative_training_set[0], negative_training_set[1], s=5, c='blue')

        axes.scatter(positive_test_set[0], positive_test_set[1], s=5, c='green')
        axes.scatter(negative_test_set[0], negative_test_set[1], s=5, c='yellow')

        # Mean Square Error Curve

        axes = figure.add_subplot(122)
        axes.set_title("MSE Curve")
        axes.set_xlabel("Epoch")
        axes.set_ylabel("MSE")

        axes.plot(range(len(self.mean_errors)), self.mean_errors, c="blue")

        plt.show()


class Layer:

    def __init__(self, neuron_count, weights_list=None, vector_cols=None, activation="sigmoid"):

        self.neuron_count = neuron_count

        self.activation = activation

        self.neurons = []

        self.vector_cols = vector_cols

        self.next_layer = None

        self.weights_list = weights_list

    def set_next_layer(self, layer):

        self.next_layer = layer

    def connect(self, last_layer=None):

        if last_layer is not None:

            self.vector_cols = last_layer.get_vector_cols()

            last_layer.set_next_layer(self)

        for index in np.arange(0, self.neuron_count):

            weights = None

            if self.weights_list:

                weights = self.weights_list[index]

            neuron = Neuron(self, index, weights=weights, activation=self.activation)

            self.neurons.append(neuron)

    def get_vector_cols(self):

        return len(self.neurons)

    def compute(self, x):

        results = []

        for neuron in self.neurons:

            results.append(neuron.compute(x))

        return np.array(results)

    def back_propagation(self, diff):

        for neuron in self.neurons:

            neuron.back_propagation(diff)

    def update(self, eta):

        for neuron in self.neurons:

            neuron.update(eta)


class Neuron:

    def __init__(self, layer, index, weights=None, activation="sigmoid"):

        self.index = index

        self.layer = layer

        if weights is not None:

            self.weights = weights

        else:

            self.weights = np.array(np.random.rand(self.layer.vector_cols))

            # self.weights = np.array(np.random.uniform(-1.0, 1.0, self.layer.vector_cols))

        self.activation = activation

        self.delta = 0.0

        self.activation_result = 0.0

        self.vector = None

    @staticmethod
    def sigmoid(x):
        return 1.0 / (1.0 + np.power(np.e, -x))

    @staticmethod
    def gard_sigmoid(x):
        return np.power(np.e, -x) / np.power(1 + np.power(np.e, -x), 2)

    def compute(self, vector):

        self.vector = vector

        self.activation_result = np.dot(self.vector, self.weights)

        if self.activation == "sigmoid":

            result = Neuron.sigmoid(self.activation_result)

        else:

            result = self.activation_result

        return float(result)

    def back_propagation(self, d):

        if self.layer.next_layer is not None:

            sum_of_terms = 0.0

            for neuron in self.layer.next_layer.neurons:

                sum_of_terms += neuron.delta * neuron.weights[self.index]
        else:

            sum_of_terms = d[self.index]

        if self.activation == "sigmoid":

            self.delta = sum_of_terms * Neuron.gard_sigmoid(self.activation_result)

        else:

            self.delta = sum_of_terms

    def update(self, eta):

        self.weights += eta * float(self.delta) * self.vector


def split_data_set(data_set, each_data_length, start_index, end_index):

    data_set = data_set.tolist()

    data_set = data_set[start_index:end_index] + data_set[each_data_length + start_index:each_data_length + end_index]

    return np.mat(data_set)

if __name__ == '__main__':

    np.random.seed(2)

    # NeuralNetwork.generate_data(use_sin=False)

    _return_tuples = NeuralNetwork.load_spiral_data()

    _training_set, _training_labels, _test_set, _test_labels = _return_tuples[:4]

    _vector_cols = _training_set.shape[1]

    network = NeuralNetwork(_training_set[:], _training_labels[:], _test_set[:], _test_labels[:], eta=0.4)

    # _layer = Layer(8, vector_cols=_vector_cols)
    #
    # network.add_layer(_layer)
    #
    # _layer = Layer(8, activation='sigmoid')
    #
    # network.add_layer(_layer)
    #
    # _layer = Layer(1, activation='sigmoid')
    #
    # network.add_layer(_layer)
    #
    # network.train(mean_error_threshold=0.05, in_sample_error_threshold=0.05, batch_size=50, max_epoch=10000)
    #
    # network.save()

    network.load()

    _results = network.test(network.test_set, network.test_labels, need_results=True)[-1]

    network.plot_spiral_scatter_and_error_curve(_return_tuples[-4].T.tolist(),
                                                _return_tuples[-3].T.tolist(),
                                                _return_tuples[-2].T.tolist(),
                                                _return_tuples[-1].T.tolist())
