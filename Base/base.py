# -*- coding:utf-8 -*-

import numpy as np


class BaseObject(object):

    def __init__(self, training_set=None, training_labels=None):

        self.training_set = np.array(training_set)

        self.training_labels = np.mat(training_labels).T.A

    @classmethod
    def load_data_set(cls, filename):

        cols = len(open(filename).readline().split('\t'))

        data_set = []

        labels = []

        fr = open(filename)

        for line in fr.readlines():

            split_line = line.strip().split('\t')

            vector = []

            for col in range(cols - 1):

                vector.append(float(split_line[col]))

            data_set.append(vector)

            labels.append(float(split_line[-1]))

        return data_set, labels
