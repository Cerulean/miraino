# -*- coding:utf-8 -*-

import json
import numpy as np
import matplotlib.pyplot as plt

from Base.base import BaseObject


class KMeans(BaseObject):

    @classmethod
    def load_data_set(cls, filename):

        data_set = []

        fr = open(filename)

        for line in fr.readlines():

            vector = map(float, line.strip().split('\t'))

            data_set.append(vector)

        return data_set

    @classmethod
    def load_blog_data(cls):

        data_set = []

        fr = open("./sample/blogdata.txt")

        line = fr.readline()

        tags = map(str, line.strip().split('\t'))

        blogs = []

        for line in fr.readlines():

            vector = line.strip().split('\t')

            blogs.append(vector[0])

            data_set.append(map(float, vector[1:]))

        return tags, blogs, np.array(data_set)

    @classmethod
    def squared_distance(cls, vector_a, vector_b):
        return np.sqrt(np.sum(np.power(vector_a - vector_b, 2)))

    def __init__(self, k, training_set=None):

        super(KMeans, self).__init__(training_set=training_set)

        self.k = k

    def generate_random_centroids(self):

        cols = self.training_set.shape[1]

        centroids_mat = np.mat(np.zeros((self.k, cols)))

        for col in range(cols):

            col_min = np.min(self.training_set[:, col])

            col_range = float(np.max(self.training_set[:, col]) - col_min)

            centroids_mat[:, col] = col_min + col_range * np.random.rand(self.k, 1)

        return centroids_mat

    def cluster(self, data_set, k):

        rows, cols = data_set.shape

        centroids_mat = self.generate_random_centroids()
        cluster_result_mat = np.mat(np.zeros((rows, 2)))

        is_cluster_changed = True

        while is_cluster_changed:

            is_cluster_changed = False

            for row in range(rows):

                min_distance = np.inf
                min_distance_centroid_index = -1

                for centroid_index in range(k):

                    vector = data_set[row, :]

                    vector_centroid = centroids_mat[centroid_index, :]

                    distance = KMeans.squared_distance(vector, vector_centroid)

                    if distance < min_distance:

                        min_distance = distance
                        min_distance_centroid_index = centroid_index

                if cluster_result_mat[row, 0] != min_distance_centroid_index:
                    is_cluster_changed = True

                cluster_result_mat[row, :] = min_distance_centroid_index, min_distance ** 2

            for index in range(k):

                result_indices = np.nonzero(cluster_result_mat[:, 0].A == index)[0]

                clustered_vectors = data_set[result_indices]

                centroids_mat[index, :] = np.mean(clustered_vectors, axis=0)

        return centroids_mat, cluster_result_mat

    def bisecting_cluster(self):

        rows = self.training_set.shape[0]

        cluster_result_mat = np.mat(np.zeros((rows, 2)))

        init_centroid = np.mean(self.training_set, axis=0)

        centroid_list = [init_centroid]

        for row in range(rows):

            vector = self.training_set[row, :]
            vector_centroid = init_centroid

            cluster_result_mat[row, 1] = KMeans.squared_distance(vector, vector_centroid) ** 2

        while len(centroid_list) < self.k:

            min_SSE = np.inf

            for centroid_index in range(len(centroid_list)):

                clustered_vectors = self.training_set[np.nonzero(cluster_result_mat[:, 0].A == centroid_index)[0]]

                split_centroids_mat, split_cluster_result_mat = self.cluster(clustered_vectors, 2)

                split_SSE = np.sum(split_cluster_result_mat[:, 1])

                origin_SSE = np.sum(cluster_result_mat[np.nonzero(cluster_result_mat[:, 0].A != centroid_index)[0], 1])

                if (split_SSE + origin_SSE) < min_SSE:

                    best_cluster_result_mat = split_cluster_result_mat.copy()
                    best_split_centroid_index = centroid_index
                    best_centroids_mat = split_centroids_mat

                    min_SSE = split_SSE + origin_SSE

            best_cluster_result_mat[np.nonzero(best_cluster_result_mat[:, 0].A == 1)[0], 0] = len(centroid_list)

            best_cluster_result_mat[np.nonzero(best_cluster_result_mat[:, 0].A == 0)[0], 0] = best_split_centroid_index

            print 'Best split centroids mat is \n', best_centroids_mat

            centroid_list[best_split_centroid_index] = best_centroids_mat[0, :]
            centroid_list.append(best_centroids_mat[1, :])

            cluster_result_mat[np.nonzero(cluster_result_mat[:, 0].A == best_split_centroid_index)[0], :] = best_cluster_result_mat

        return centroid_list, cluster_result_mat


if __name__ == '__main__':

    from sqlite3 import dbapi2 as sqlite

    connection = sqlite.connect('./sample/test.db')

    # connection.execute('CREATE TABLE people(name, gender, city)')

    # connection.execute('INSERT INTO people VALUES (\'Shuyu\', \'Male\', \'China\')')

    connection.commit()

    cursor = connection.execute('SELECT name, gender FROM people WHERE city = \'China\'')

    for row in cursor:

        print row

    connection.execute('UPDATE people SET city = \'Kitakyushu\' WHERE name = \'Shuyu\'')

    connection.commit()

    cursor = connection.execute('SELECT * FROM people')

    for row in cursor:

        print row

    # _tags, _blogs, _data_set = KMeans.load_blog_data()
    #
    # means = KMeans(20, training_set=_data_set)
    #
    # _centroid_list, _cluster_result_mat = means.cluster(_data_set, 20)
    #
    # clusters = {}
    #
    # for index in range(len(_blogs)):
    #
    #     blog = _blogs[index]
    #
    #     clusters[blog] = _cluster_result_mat[index, 0]
    #
    # fr = open('./sample/result.json', 'w')
    #
    # json.dump(clusters, fr, indent=True)